# Speedrunner #

### What is this repository for? ###

This code demonstrates the use of a bicriterion integer programming (IP) model to solve a [puzzle](https://math.stackexchange.com/questions/4569317/how-can-i-solve-this-optimization-problem-with-integer-variables) posted on Mathematics Stack Exchange. The question references a video game named "speedrunner" and looks to find a strategy that will "earn $1 billion of in-game currency in the shortest possible time". 

The integer programming model has a primary objective of minimizing elapsed time and a secondary objective of maximizing winnings (by minimizing the negative of the logarithm of winnings). The objectives are specified lexicographically, meaning elapsed time has highest priority and winnings are improved only so long as elapsed time is not adversely affected. Details of the model formulation are presented in a [blog post](https://orinanobworld.blogspot.com/2022/11/a-bicriterion-ip-model-for-game-strategy.html).

### Details ###

The code was developed using CPLEX 22.1 but should run with version 12.10 or later.

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

