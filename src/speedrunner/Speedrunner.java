package speedrunner;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloObjective;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplexMultiCriterionExpr;

/**
 * Speedrunner uses an integer programming model to solve an optimization
 * problem posted on Mathematics Stack Exchange.
 *
 * https://math.stackexchange.com/questions/4569317/how-can-i-solve-this
 * -optimization-problem-with-integer-variables
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Speedrunner {

  /**
   * Dummy constructor.
   */
  private Speedrunner() { }

  /**
   * Sets up and solves the integer programming model.
   *
   * Note: Because some original variables have a lower bound of 1 and Java
   * uses 0-based indexing, we create an extra element in certain variable
   * arrays with index 0 and lock the corresponding variable at 0.
   *
   * @param args the command line arguments (ignored)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Set the problem parameters (using the original notation where possible).
    double x0 = 2.5e4;
    double ta = 52 / 60.;   // converted to minutes
    double tb = 18 / 60.;   // converted to minutes
    double tc = 600 / 60.;  // converted to minutes
    double minWinnings = 1e9;
    // Choose an upper bound for elapsed time (objective).
    double tMax = 60.;      // minutes
    // Estimate upper bounds on the original integer variables.
    int naMax = (int) Math.floor((tMax - tb - tc) / ta);
    int nbMax = (int) Math.floor((tMax - ta - tc) / tb);
    int ncMax = (int) Math.floor((tMax - ta) / (tb + tc));
    System.out.println("Using upper bound " + tMax + " min. for objective");
    System.out.println("Upper bound on n_a = " + naMax);
    System.out.println("Upper bound on n_{b,i} = " + nbMax);
    System.out.println("Upper bound on n_c = " + ncMax);
    // Set up and solve the MIP model.
    try (IloCplex mip = new IloCplex()) {
      // Create variables.
      IloNumVar na = mip.intVar(1, naMax, "na");
      IloNumVar nc = mip.intVar(1, ncMax, "nc");
      IloNumVar[] nb = new IloNumVar[ncMax + 1];
      for (int i = 0; i <= ncMax; i++) {
        nb[i] = mip.intVar(0, nbMax, "nb_" + i);
      }
      IloNumVar[] y = new IloNumVar[naMax + 1];
      for (int i = 0; i <= naMax; i++) {
        y[i] = mip.boolVar("y_" + i);
      }
      y[0].setUB(0);
      IloNumVar[] w = new IloNumVar[ncMax + 1];
      for (int i = 0; i <= ncMax; i++) {
        w[i] = mip.boolVar("w_" + i);
      }
      w[0].setUB(0);
      IloNumVar[][] z = new IloNumVar[ncMax + 1][nbMax + 1];
      for (int i = 0; i <= ncMax; i++) {
        for (int j = 0; j <= nbMax; j++) {
          z[i][j] = mip.boolVar("z_" + i + "_" + j);
          if (i == 0) {
            z[i][j].setUB(0);
          }
        }
      }
      // Define n_a.
      IloLinearNumExpr expr = mip.linearNumExpr();
      for (int j = 1; j <= naMax; j++) {
        expr.addTerm(j, y[j]);
      }
      mip.addEq(na, expr, "n_a__def");
      mip.addEq(mip.sum(y), 1.0, "n_a__SOS");
      // Define n_c.
      expr = mip.linearNumExpr();
      for (int j = 1; j <= ncMax; j++) {
        expr.addTerm(j, w[j]);
      }
      mip.addEq(nc, expr, "n_c__def");
      mip.addEq(mip.sum(w), 1.0, "n_c__SOS");
      // Define n_{b,i}.
      for (int i = 1; i <= ncMax; i++) {
        expr = mip.linearNumExpr();
        for (int j = 0; j <= nbMax; j++) {
          expr.addTerm(j, z[i][j]);
        }
        mip.addEq(nb[i], expr, "n_b_" + i + "_def");
        mip.addEq(mip.sum(z[i]), 1.0, "n_b_" + i + "_SOS");
        // n_{b,i} = 0 iff n_c < i.
        expr = mip.linearNumExpr();
        expr.addTerm(1.0, z[i][0]);
        for (int k = i; k <= ncMax; k++) {
          expr.addTerm(1.0, w[k]);
        }
        mip.addEq(expr, 1.0, "n_b_" + i + "__zero");
      }
      // Add the constraint on minimal winnings.
      IloLinearNumExpr logWin = mip.linearNumExpr(Math.log(x0));
      for (int i = 1; i <= naMax; i++) {
        logWin.addTerm(Math.log(i), y[i]);
      }
      for (int i = 1; i <= ncMax; i++) {
        for (int j = 1; j <= nbMax; j++) {
          logWin.addTerm(Math.log(j), z[i][j]);
        }
      }
      mip.addGe(logWin, Math.log(minWinnings), "winnings");
      // Add antisymmetry constraints.
      for (int i = 1; i < ncMax - 1; i++) {
        mip.addGe(nb[i], nb[i + 1]);
      }
      // Set the objective function. The highest priority is to minimize
      // elapsed time. The secondary priority is to maximizing winnings by
      // minimizing the negative of the log of winnings.
      IloNumExpr time = mip.sum(mip.prod(ta, na), mip.prod(tc, nc));
      time = mip.sum(time, mip.prod(tb, mip.sum(nb)));
      IloCplexMultiCriterionExpr objVec =
        mip.staticLex(new IloNumExpr[] {time, mip.prod(-1.0, logWin)});
      IloObjective obj = mip.minimize(objVec);
      mip.add(obj);
      // Solve the model.
      mip.solve();
      System.out.println("\nMinimum time required = " + mip.getObjValue()
                         + " minutes.");
      System.out.println("Solution:");
      double q = mip.getValue(na);
      System.out.println("\tn_a = " + Math.round(q));
      q = mip.getValue(nc);
      int c = (int) Math.round(q);
      System.out.println("\tn_c = " + c);
      double[] q2 = mip.getValues(nb);
      for (int i = 1; i <= c; i++) {
        int c0 = (int) Math.round(q2[i]);
        System.out.println("\tn_{b," + i + "} = " + c0);
      }
      // Get the winnings.
      double win = Math.exp(mip.getValue(logWin));
      System.out.println("Estimated winnings = " + win);
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
  }

}
